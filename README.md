# Progetto Abramo Maffei pt.1 PR2
Il progetto prevede come richiesto l'implementazione di una interfaccia chiamata SecureDataContainer<E>
dove è caratteristica la necessità di utilizzare i "generali" <E>.

## Dati Studente
	Nome: Abramo
	Cognome: Maffei
	N.Matricola: 552887
	Corso: A



## Il progetto è diviso in due parti:
### 1)ArrayList part:
In cui il programma si occupera di gestire due ArrayList una per la memorizzazione degli "User" (DataSecure) e un'altra per le gestione dei "Data" ovvero i generali che gli verranno passati (User).
La scelta dell'ArrayList come collection è basata puramente sulla necessità di avere un tipologia di Array dinamico dato che non abbiamo la necessità di definire immediatamente una dimensione fisica predefinita.
	
### 2)LinkeList + TreeMap part:
Questa parte del programma avrà presso che le stesse funzionalità della prima parte, ma utilizza strutture più specifiche per una gestione migliore.
In questo caso la gestione degli "User" verrà affidata al TreeMap in cui utilizzeremo la chiave della coppia <chiave,valore> come id/nome dell'User partendo dal presupposto che in questo programma tutti gli id saranno diversi (l'id si identifica anche come nome+cognome+codiceseriale).
Mentre come valore gli passeremo un oggetto di una classe SecurePassword al cui interno sarà contenuta la password e un LinkedList di oggetti generici.
La scelta di una LinkedList è nata dalla necessità di inserirvi appunto oggetti generici di grande numero per tanto la tipologia di tale collezione favorisce l'aggiunta e la modifica di tali oggetti generici consentendo una maggiore libertà e flessibilità.
	
## Batterie di test:
Inizialmente il programma esegue dei test sulla prima parte del progetto eseguendo i principali comandi di aggiunta, modifica, rimozione e
condivisione collegati da una stampa su console dei risultati parziali ad ogni azione, principalmente passando come oggeti delle String 
per poi utilizzare anche un oggetto appositamente creato "Book".

Dopo di che si entra nella seconda parte in cui vengono eseguiti anche qua i principali comandi però questa volta solamente riutilizzando
i "Book".

## Specifiche di utilizzo:
La funzione toString() nella seconda parte è stata riscritta per una maggiore compresione degli oggetti, inoltre è stata anche riscritta
per la classe "Book".
	

