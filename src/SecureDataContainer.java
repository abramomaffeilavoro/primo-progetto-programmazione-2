//INTERFACCIA SECUREDATACONTAINER
import java.util.Iterator;

public interface SecureDataContainer<E>{
	//OVERVIEW: Interfaccia che gestisce dati generici di utenti autenticati con Id e passw
	//TIPICAL ELEMENT: 
	//	<Id_0, passw_0, {data_0_0, ... , data_0,m}> , .... ,  <Id_n, passw_n, {data_n_0, ... , data_n_m>.
	//	Id_i != Id_x per ogni x,i. 0 <= i < n && 0 <= x < n
	//	data_x_y = dato di Id_x con indice y
	//	n = numero di Utenti, 0<=n && n!=null
	//	Id && passw sono String
	
	
	// Crea l�identit� un nuovo utente della collezione
	public void createUser(String Id, String passw) throws NullPointerException, IllegalArgumentException;
	//@REQUIRES: Id != null && passw != null
	//@MODIFIES: this
	//@THROWS: se Id==null || passw == null lancia NullPointerException
	//		   se Id � gi� presente lancia IllegalArgumentException
	//@EFFECTS: aggiunge un nuovo Utente a this
	
	
	// Rimuove l�utente dalla collezione
	public void RemoveUser(String Id, String passw) throws NullPointerException, IllegalArgumentException;
	//@REQUIRES: Id != null && passw != null
	//@MODIFIES: this
	//@THROWS: se Id == null || passw == null lancia NullPointerException
	//		   se Id non � presente lancia IllegalArgumentException
	//		   se passw � sbagliata lancia IllegalArgumentException
	//@EFFECTS: rimuove l'utente da this
	
	
	// Restituisce il numero degli elementi di un utente presenti nella collezione
	public int getSize(String Owner, String passw) throws NullPointerException, IllegalArgumentException;
	//@REQUIRES: Owner != null && passw != null
	//@THROWS: se Owner == null || passw == null lancia NullPointerException
	//         se Owner non � presente lancia IllegalArgumentException
	//         se passw � sbagliata lancia IllegalArgumentException
	//@RETURN: restituisce il numero di Utenti presenti nella collezione com Intero
	
	
	// Inserisce il valore del dato nella collezione se vengono rispettati i controlli di identit�
	public boolean put(String Owner, String passw, E data) throws NullPointerException;
	//@REQUIRES: Owner != null && passw != null && data != null
	//@MODIFIES: this
	//@THROWS: se Owner == null || passw == null || data == null lancia NullPointerException
	//@EFFECTS: Inserisce il valode del dato nella collezione
	//@RETURN: restituisce true se l'upload � andato a buon fine, false altrimenti.
	
	
	// Ottiene una copia del valore del dato nella collezione se vengono rispettati i controlli di identit�
	public E get(String Owner, String passw, E data) throws NullPointerException;
	//@REQUIRES: Owner != null && passw != null && data != null
	//@THROWS: se Owner == null || passw == null || data == null lancia NullPointerException
	//@RETURN: restituisce una copia del valore del dato nella collezione.
	
	
	// Rimuove il dato nella collezione se vengono rispettati i controlli di identit�
	public E remove(String Owner, String passw, E data) throws NullPointerException;
	//@REQUIRES: Owner != null && passw != null && data != null
	//@MODIFIES: this
	//@THROWS: se Owner == null || passw == null || data == null lancia NullPointerException
	//@EFFECTS: Rimuove il dato nella collezione
	//@RETURN: restituisce l'elemento eliminato dalla collezione
	
	// Crea una copia del dato nella collezione se vengono rispettati i controlli di identit�
	public void copy(String Owner, String passw, E data) throws NullPointerException, IllegalArgumentException;
	//@REQUIRES: Owner != null && passw != null && data != null
	//@MODIFIES: this
	//@THROWS: se Owner == null || passw == null || data == null lancia NullPointerException, 
	//		   se Owner non � presente lancia IllegalArgumentException
	//	  	   se passw � sbagliata lancia IllegalArgumentException
	//	       se data non � presente lancia IllegalArgumentException
	//@EFFECTS: Fa una copia del dato all'interno della collezione di dati
	
		
	
	// Condivide il dato nella collezione con un altro utente se vengono rispettati i controlli di identit�
	public void share(String Owner, String passw, String Other, E data) throws NullPointerException;
	//@REQUIRES: Owner != null && passw != null && data != null || Other != null
	//@MODIFIES: this
	//@THROWS: se Owner == null || passw == null || data == null lancia NullPointerException, 
	//		   se Owner non � presente lancia IllegalArgumentException
	//	       se Other che � un Id non � presente lancia IllegalArgumentException
	//	  	   se passw � sbagliata lancia IllegalArgumentException
	//	       se data non � presente lancia IllegalArgumentException
	//@EFFECTS: Fa una copia del dato presente nella collezione di Owner dentro la collezione di Other
	 
	
	// restituisce un iteratore (senza remove) che genera tutti i dati dell�utente in ordine arbitrario se vengono rispettati i controlli di identit�
	public Iterator<E> getIterator(String Owner, String passw) throws NullPointerException;
	//@REQUIRES: Id != null && passw != null && data != null
	//@THROWS: se Owner == null || passw == null lancia NullPointerException
	//@RETURN: ritorna la collezione di dati
}