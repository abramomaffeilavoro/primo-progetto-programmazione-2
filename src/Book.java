//STRUTTURA DATI BOOK SOLO PER TEST
public class Book {
	//OVERVIEW: un Book � un Oggetto composto da titolo(title), autore(author) e tipo(type)
	//TIPICAL ELEMENT: <title,author,type> es. <"Il suono in cui viviamo","Franco Fabbri","Music">
	
	private String title;
	private String author;
	private String type;
	
	public Book(String _title, String _author, String _type) throws NullPointerException{
		//@REQUIRES: _title != null &&  _author != null && _type != null
		//@THROWS: se _title == null || _author == null || _type == null lacia NullPointerException
		//@EFFECTS: crea un nuovo Book.
		if(_title == null || _author == null || _type == null) throw new NullPointerException();
		this.title = _title;
		this.author = _author;
		this.type = _type;
	}

	public String getTitle() {
		//@RETURN: <String>title
		return title;
	}

	public String getAuthor() {
		//@RETURN: <String>author
		return author;
	}

	public String getType() {
		//@RETURN: <String>type
		return type;
	}
	
	public String toString() {
		//@RETURN: una stringa composta da title, author, type
		return "[Title: "+ title +", Author: " + author +", Type: "+ type +"]";
	}
}
