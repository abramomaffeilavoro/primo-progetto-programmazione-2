//CLASSE AUSILIARIA SECUREPASSWORD

import java.util.Iterator;
import java.util.LinkedList;

public class SecurePassword<E> {
	//OVERVIEW: SecurePassword � un Oggetto di tipo generico composto da password e un LinkedList(data).
	//TIPICAL ELEMENT: <password_0, <data_0_0, ... , data_0_n>
	
	private String password;
	private LinkedList<E> data;
	
	public SecurePassword(String _passw) {
		//@REQUIRES:  _passw
		//@EFFECTS: crea un nuovo SecurePassword
		this.password = _passw;
		this.data = new LinkedList<E>();
	}
	
	public E get(E _item) {
		//@REQUIRES:  <E>_item
		//@RETURN: ritorna l'elemento richiesto
		return data.get(data.indexOf(_item));
	}
	
	public String getPassw() {
		//@RETURN: <String>password
		return this.password;
	}
	
	public int size() {
		//@RETURN: <Integer>data.size()
		return data.size();
	}
	
	public boolean checkPassw(String _passw) {
		//@REQUIRES: _passw
		//@RETURN: true se this.password == _passw, false altrimenti
		if(this.password == _passw) {return true;}
		return false;
	}
	
	public boolean add(E _item) {
		//@REQUIRES: <E>_item
		//@MODIFIES: this.add
		//@EFFECTS: aggiunge _item ad data
		//@RETURN: true se viene aggiunto, false altrimenti
		return data.add(_item);
	}
	
	public void destroy() {
		//@MODIFIES: this
		//@EFFECTS: imposta password = null e pulisce data
		this.password = null;
		this.data.clear();
	}
	
	public E removeData(E _data) {
		//@REQUIRES: <E>_data
		//@MODIFIES: this.data
		//@EFFECTS: rimuove _data dalla collezione
		//@RETURN: ritorna l'oggetto eliminato
		return data.remove(data.indexOf(_data));
	}
		
	public boolean contains(E obj) {
		//@REQUIRES: <E>obj
		//@RETURN: true se data contiene obj, false altrimenti
		return data.contains(obj);
	}

	public Iterator<E> getIterator() {
		//@RETURN: ritorna un iteratore sulla collezione di data
		ReadOnlyIterator<E> tmp = new ReadOnlyIterator<E>(this.data.iterator());
		return tmp;
	}
}
