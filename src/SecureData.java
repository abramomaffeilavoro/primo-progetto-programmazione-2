
//STRUTTURA DATI SECUREDATA

import java.util.ArrayList;
import java.util.Iterator;

public class SecureData<E> implements SecureDataContainer<E>{
	
	private ArrayList< User<E> > users;
	//FUNZIONE DI ASTRAZIONE
	//	{ <Id_0, passw_0, {data_0_0, ... , data_0,m}> , .... ,  <Id_n, passw_n, {data_n_0, ... , data_n_m> }
	
	//INVARIANTE DI RAPPRESENTAZIONE:
	//	User != null && User.data.size() >= 0 &&
	//		per ogni x,y | 0 <= x < User.data.size() && x != y ==> users.get(x).getId() != users.get(y).getId()
	//		per ogni x | users.get(x).data != null
	
	
	public SecureData() {
		//Inizializzo l'ArrayList di User.
		this.users = new ArrayList<User<E>>();
	}
	
	
	
	
	@Override
	public void createUser(String _Id, String _passw) throws NullPointerException, IllegalArgumentException {
		//Controllo varlori nulli.
		if(_Id == null || _passw == null) throw new NullPointerException();
		//Utilizzo il metodo sul for per scorrere tutto l'ArrayList "users" per controllare se esiste gi� l'id.
		for(User<E> u: this.users) {
			if(u.getId() == _Id) {
				throw new IllegalArgumentException("Utente gi� esistente");
			}
		}
		//Creo il nuovo user.
		User<E> x = new User<E>(_Id , _passw);
		//Lo aggiungo all' ArrayList.
		this.users.add(x);
	}

	
	
	@Override
	public void RemoveUser(String _Id, String _passw) throws NullPointerException, IllegalArgumentException {
		//Controllo varlori nulli.
		if(_Id == null || _passw == null) throw new NullPointerException();
		//Utilizzo il metodo sul for per scorrere tutto l'ArrayList "users" per controllare se esiste l'id.
		for(User<E> u: this.users) {
			if(u.check(_Id, _passw)){
				//Utente trovato.
				u.destroy(_Id, _passw); //Richiamo la funzione di autoeliminazione dell'ogggeto User.
				users.remove(u); //Rimuovo anche l'oggetto dall'ArrayList users.
				return;
			}
		}
		throw new IllegalArgumentException("Utente non trovato");
	}

	
	
	@Override
	public int getSize(String _Owner, String _passw) throws NullPointerException, IllegalArgumentException {
		//Controllo varlori nulli.
		if(_Owner == null || _passw == null) throw new NullPointerException();
		//Utilizzo il metodo sul for per scorrere tutto l'ArrayList "users" per controllare se esiste l'id.
		for(User<E> u: this.users) {
			if(u.check(_Owner, _passw)){
				//Utente trovato.
				return u.size();
			}
		}
		throw new IllegalArgumentException("Utente non trovato");
	}

	
	
	
	@Override
	public boolean put(String _Owner, String _passw, E _data) throws NullPointerException {
		//Controllo varlori nulli.
		if(_Owner == null || _passw == null || _data == null) throw new NullPointerException();
		//Utilizzo il metodo sul for per scorrere tutto l'ArrayList "users" per controllare se esiste l'id.
		for(User<E> u: this.users) {
			if(u.check(_Owner, _passw)){
				//Utente trovato.
				u.add(_data); //Dato inserito.
				return true;
			}
		}
		return false;
		
	}

	
	
	
	@Override
	public E get(String _Owner, String _passw, E _data) throws NullPointerException {
		//Controllo varlori nulli.
		if(_Owner == null || _passw == null || _data == null) throw new NullPointerException("Dati mancanti");
		//Utilizzo il metodo sul for per scorrere tutto l'ArrayList "users" per controllare se esiste l'id.
		for(User<E> u: this.users) {
			if(u.check(_Owner, _passw)){
				//Utente trovato.
				if(!u.contains(_data)){throw new IllegalArgumentException("Elemento non presente nella collezione");}
				return u.get(_data);
			}
		}
		throw new IllegalArgumentException("Utente non trovato");
	}

	
	
	
	@Override
	public E remove(String _Owner, String _passw, E _data) throws NullPointerException {
		//Controllo varlori nulli.
		if(_Owner == null || _passw == null || _data == null) throw new NullPointerException("Dati mancanti");
		//Utilizzo il metodo sul for per scorrere tutto l'ArrayList "users" per controllare se esiste l'id.
		for(User<E> u: this.users) {
			if(u.check(_Owner, _passw)){
				//Utente trovato.
				//Controllo che esista l'elemento.
				if(u.contains(_data)) {
					return u.removeData(_data); //Elimino e lo ritorno.
				}
				throw new IllegalArgumentException("Elemento non presente nella collezione");
			}
		}
		throw new IllegalArgumentException("Utente non trovato");
	}

	
	
	
	@Override
	public void copy(String _Owner, String _passw, E _data) throws NullPointerException, IllegalArgumentException {
		//Controllo varlori nulli.
		if(_Owner == null || _passw == null || _data == null) throw new NullPointerException("Dati mancanti");
		//Utilizzo il metodo sul for per scorrere tutto l'ArrayList "users" per controllare se esiste l'id.
		for(User<E> u: this.users) {
			if(u.check(_Owner, _passw)){
				//Utente trovato.
				//Controllo che esista l'elemento.
				if(!u.contains(_data)) {throw new IllegalArgumentException("Elemento non presente nella collezione");}
				u.add(u.get(_data)); //Lo riaggiungo.
				return;
			}
		}
		throw new IllegalArgumentException("Utente non trovato");
		
	}

	
	
	
	
	@Override
	public void share(String _Owner, String _passw, String _Other, E _data) throws NullPointerException, IllegalArgumentException {
		//Controllo varlori nulli.
		if(_Owner == null || _passw == null || _data == null || _Other == null) throw new NullPointerException("Dati mancanti");
		for(User<E> u: this.users) {
			if(u.check(_Owner, _passw)){
				if(u.contains(_data)) {
					//Credenziali e dati controllati ora lo condividiamo con Other.
					for(User<E> u2: this.users) {
						if(u2.getId() == _Other) {
							u2.add(u.get(_data)); //Dato aggiunto all' altro utente.
							return;
						}
					}
					throw new IllegalArgumentException("Altro Utente non trovato");
				}
				throw new IllegalArgumentException("Elemento non presente nella collezione");
			}
		}
		throw new IllegalArgumentException("Utente non trovato");
		
	}

	
	
	

	@Override
	public Iterator<E> getIterator(String _Owner, String _passw) throws NullPointerException {
		//Controllo varlori nulli.
		if (_Owner == null || _passw == null) throw new NullPointerException();
		//Controllo la non presenza di ID.
		for(User<E> u: this.users) {
			//Controllo la password.
			if(u.check(_Owner, _passw)) {
				return u.getIterator();
			}else {
				throw new IllegalArgumentException("Credenziali Errate");
			}
		}
		return null;
	}

}
