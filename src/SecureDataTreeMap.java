
//STRUTTURA DATI SECUREDATATREEMAP.


import java.util.Iterator;
import java.util.TreeMap;

public class SecureDataTreeMap<E> implements SecureDataContainer<E> {
	private TreeMap<String,SecurePassword<E>> list;
	//FUNZIONE DI ASTRAZIONE
	//	{ <Id_0, passw_0, {data_0_0, ... , data_0,m}> , .... ,  <Id_n, passw_n, {data_n_0, ... , data_n_m> }
	
	//INVARIANTE DI RAPPRESENTAZIONE:
	//	SecurePassword != null && SecurePassword.size() >= 0 &&
	//		per ogni x,y | 0 <= x < SecurePassword.size() && x != y ==> SecurePassword.get(x).getId() != SecurePassword.get(y).getId()
	//		per ogni x | SecurePassword.get(x).data != null
	
	
	public SecureDataTreeMap() {
		//Inizializzo list
		list = new TreeMap<String,SecurePassword<E>>();
	}

	@Override
	public void createUser(String _Id, String _passw) throws NullPointerException, IllegalArgumentException {
		//Controllo varlori nulli.
		if(_Id == null || _passw == null) { throw new NullPointerException(); }
		//Controllo la presenza di ID
		if(list.containsKey(_Id)) {throw new IllegalArgumentException("ID gi� presente"); }
		//Creo l'oggeto val della classe SecurePassword
		SecurePassword<E> val = new SecurePassword<>(_passw);
		//Aggiungo alla TreeMap il nuovo elemento
		list.put(_Id, val);
		System.out.println("--> User Created: [" + _Id + " , " + val.getPassw() +"]");
		
	}

	@Override
	public void RemoveUser(String _Id, String _passw) throws NullPointerException, IllegalArgumentException {
		//Controllo varlori nulli
		if(_Id == null || _passw == null) { throw new NullPointerException(); }
		//Controllo la NON presenza di ID
		if(!list.containsKey(_Id)) {throw new IllegalArgumentException("ID non trovato"); }
		//Controllo la password
		if(list.get(_Id).checkPassw(_passw)==false) {throw new IllegalArgumentException("Password sbagliata");}
		//Richiamo la funzione destroy per eliminare l'oggetto e la LinkedList associata
		list.get(_Id).destroy();
		//Rimuovo dalla TreeMap l'oggetto
		list.remove(_Id);
		System.out.println("<-- User Removed: [ " + _Id + " ]");
		
	} 

	@Override
	public int getSize(String _Owner, String _passw) throws NullPointerException, IllegalArgumentException {
		//Controllo varlori nulli
		if(_Owner == null || _passw == null) { throw new NullPointerException(); }
		//Controllo la NON presenza di ID
		if(!list.containsKey(_Owner)) {throw new IllegalArgumentException("ID non trovato"); }
		//Controllo la password
		if(list.get(_Owner).checkPassw(_passw)==false) {throw new IllegalArgumentException("Password sbagliata");}
		//se sono qua dati not null & id trovato nella lista & password controllata
		return list.get(_Owner).size();
	}

	@Override
	public boolean put(String _Owner, String _passw, E _data) throws NullPointerException {
		//Controllo varlori nulli
		if(_Owner == null || _passw == null || _data == null) { throw new NullPointerException(); }
		//Controllo la NON presenza di ID
		if(!list.containsKey(_Owner)) {throw new IllegalArgumentException("ID non trovato");}
		//Controllo la password
		if(list.get(_Owner).checkPassw(_passw)==false) {throw new IllegalArgumentException("Password sbagliata");}
		//se sono qua dati not null & id trovato nella lista & password controllata
		//Richiamo l'add per aggiungere un oggetto alla LinkeList
		if(list.get(_Owner).add(_data)) {
			System.out.println("+ Object Added: [to: " + _Owner + " as: " + list.get(_Owner).get(_data).toString() + " ]") ;
			return true;
			}
		return false;
	}

	@Override
	public E get(String _Owner, String _passw, E _data) throws NullPointerException {
		//Controllo varlori nulli
		if(_Owner == null || _passw == null || _data == null) { throw new NullPointerException(); }
		//Controllo la non presenza di ID
		if(!list.containsKey(_Owner)) {throw new IllegalArgumentException("ID non trovato");}
		//Controllo la password
		if(list.get(_Owner).checkPassw(_passw)==false) {throw new IllegalArgumentException("Password sbagliata");}
		if(!list.get(_Owner).contains(_data)) {throw new IllegalArgumentException("Oggetto non trovato");}
		//se sono qua dati not null & id trovato nella lista & password controllata e _data � presente
		return list.get(_Owner).get(_data);
		
	}

	@Override
	public E remove(String _Owner, String _passw, E _data) throws NullPointerException {
		//Controllo varlori nulli
		if(_Owner == null || _passw == null || _data == null) { throw new NullPointerException(); }
		//Controllo la NON presenza di ID
		if(!list.containsKey(_Owner)) {throw new IllegalArgumentException("ID non trovato");}
		//Controllo la password
		if(list.get(_Owner).checkPassw(_passw)==false) {throw new IllegalArgumentException("Password sbagliata");}
		//Richiamo removeData() per la rimozioni di un elemento dalla LinkedList
		if(!list.get(_Owner).contains(_data)) {throw new IllegalArgumentException("Oggetto non trovato");}
		return list.get(_Owner).removeData(_data);
	}

	@Override
	public void copy(String _Owner, String _passw, E _data) throws NullPointerException, IllegalArgumentException {
		//Controllo varlori nulli
		if(_Owner == null || _passw == null || _data == null) { throw new NullPointerException(); }
		//Controllo la NON presenza di ID
		if(!list.containsKey(_Owner)) {throw new IllegalArgumentException("ID non trovato");}
		//Controllo la password
		if(list.get(_Owner).checkPassw(_passw)==false) {throw new IllegalArgumentException("Password sbagliata");}
		//Controllo che l'oggetto esista nella LinkedList
		if(!list.get(_Owner).contains(_data)) {throw new IllegalArgumentException("Oggetto non trovato"); }
		//Lo copio in una variabile generica
		E toCopy = list.get(_Owner).get(_data);
		//Lo aggiungo nuovamente
		list.get(_Owner).add(toCopy);
		System.out.println("+ Object Added: [to: " + _Owner + " as: " + list.get(_Owner).get(_data).toString() + " ]") ;
		
		
	}

	@Override
	public void share(String _Owner, String _passw, String _Other, E _data) throws NullPointerException {
		//Controllo varlori nulli
		if(_Owner == null || _passw == null || _data == null || _Other == null) { throw new NullPointerException(); }
		//Controllo la NON presenza di ID
		if(!list.containsKey(_Owner)) {throw new IllegalArgumentException("ID Owner non trovato");}
		//Controllo la password
		if(list.get(_Owner).checkPassw(_passw)==false) {throw new IllegalArgumentException("Password sbagliata");}
		//Controllo la NON presenza del secondo ID
		if(!list.containsKey(_Other)) {throw new IllegalArgumentException("ID Other non trovato");}
		//Controllo la presenza dell'oggetto 
		if(!list.get(_Owner).contains(_data)) {throw new IllegalArgumentException("Oggetto non trovato"); }
		//Lo assegno a una variabile
		E toShare = list.get(_Owner).get(_data);
		//Lo aggiungo alla LinkedList dell'altro ID
		list.get(_Other).add(toShare);
		System.out.println("+ Object Shared: [from: "+ _Owner + " to: " + _Other + " as: " + list.get(_Other).get(_data).toString() + " ]") ;
	}
 
	@Override
	public Iterator<E> getIterator(String _Owner, String _passw) throws NullPointerException, IllegalArgumentException {
		//Controllo varlori nulli
		if (_Owner == null || _passw == null) throw new NullPointerException();
		//Controllo la non presenza di ID
		if(!list.containsKey(_Owner)) {throw new NullPointerException();}
		//Controllo la password
		if(list.get(_Owner).checkPassw(_passw)==false) {throw new IllegalArgumentException("Password sbagliata");}
		SecurePassword <E> val = this.list.get(_Owner);
		return val.getIterator();
		
	}

}
