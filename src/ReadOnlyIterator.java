import java.util.Iterator;

public class ReadOnlyIterator <E> implements Iterator<E> {
	//Viene implementato una versione dell'Iterator senza la possibilitÓ di rimozione --> READONLY.
	private Iterator <E> base;
	
	public ReadOnlyIterator(final Iterator <E> it) {
		//@REQUIRES: Iterator <E>it
		//@EFFECTS: Crea l'iteratore
		//@THROWS: Se it == null allora restituisce NullPointerException
		if(it == null) {
			throw new NullPointerException("Valore Passato nullo");
		}
		this.base = it;
	}

	@Override
	public boolean hasNext() {
		//@RETURN: True se esiste un prossimo iteratore, false altrimenti.
		return this.base.hasNext();
	}

	@Override
	public E next() {
		//@RETURN: Ritorna il prossimo iteratore.
		return this.base.next();
	}

}
