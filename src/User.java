//CLASSE AUSILIARIA USER.

import java.util.ArrayList;
import java.util.Iterator;

public class User<E>{
	//OVERVIEW: un User � un Oggetto di tipo generico composto da id, password e un ArrayList(data).
	//TIPICAL ELEMENT: <id_0, password_0, <data_0_0, ... , data_0_n>
	
	private String id;
	private String password;
	private ArrayList<E> data;

	
	public User(String _id, String _password) {
		//@REQUIRES: _id && _password
		//@EFFECT: viene creato un nuovo User
		this.id = _id;
		this.password = _password;
		
		//inizializzo l'ArrayList
		this.data = new ArrayList<E>();
		System.out.println("--> User Created: [" + _id + " , " + _password +"]");
	}
	
	//Getter di id
	public String getId() {
		//@RETURN: <String>id
		return this.id;
	}	
	
	//Aggiungo un elemento generico E all ArrayList
	public void add(E obj) {
		//@REQUIRES: <E> obj
		//@MODIFES: this.data
		//@EFFECTS: aggiunge <E>obj a data
		if(this.data.add(obj)){
			System.out.println("+ Object Added: [to: " + id + ", as: " + obj.toString() +"]");
		}
	}
	
	//Prendo un elemento specifico E dall'ArrayList e lo restituisco
	public E get(E obj) {
		//@REQUIRES: <E>obj
		//@RETURN: <E>data.get(data.indexOf(obj))
		return data.get(data.indexOf(obj));
	}
	
	public int size() {
		//@RETURN data.size()
		return data.size();
	}
	
	public boolean contains(E obj) {
		//@REQUIRES: <E>obj
		//@RETURN: true se data contiene obj, false altrimenti
		return data.contains(obj);
	}
	
	//Rimuovo un elemento Elemento specifico E dall'ArrayList e restituisco true se � stato cancellato false altrimenti
	public E removeData(E obj) {
		//@REQUIRES: <E>obj
		//@MODIFES: this.data
		//@EFFECTS: rimuove <E>obj a data
		//@RETURN: ritorna l'elemento rimosso
			return data.remove(data.indexOf(obj));
	}
	
	//controllo sulla validit� dei dati
	public boolean check(String _id , String _pass) throws IllegalArgumentException {
		//@REQUIRES: _id && _pass
		//@THROWS: if id==_id && password!=_pass lancia IllegalArgumentException
		//@RETURN: true if this.id == _id && this.password == _pass
		if((this.id==_id)&&(this.password != _pass)) {throw new IllegalArgumentException("Password Sbagliata");}
		if((this.id==_id)&&(this.password == _pass)) {return true;}
		return false;
		
	}
	
	
	//Autoeliminazione
	public void destroy(String _id, String _pass) {
		//@REQUIRES: _id && _pass
		//@MODIFES: this
		//@EFFECT: imposta id = null, password = null e pulisce tutto l'array
			//imposto tutto su null per consentire al Garbage Collector di eliminare l'oggetto
			this.id = null;
			this.password = null;
			data.clear(); //pulisco l'ArrayList
	}
		
	//Setto l'iterator
	public Iterator <E> getIterator(){
		//@RETURN: ritorna un iteratore sulla collezione
		ReadOnlyIterator<E> tmp = new ReadOnlyIterator<E>(this.data.iterator());
		return tmp;
	}
	
	


	
}
