import java.util.Iterator;

class Main {

	public static void main(String[] args) {
		
/*----------------------------------------PRIMA PARTE---------------------------------------------------*/
		
		System.out.println("<------------------------ PRIMA PARTE ------------------------->");
		//creiamo collection utilizzando la classe SecureData
		SecureData <String> collection = new SecureData<String>();
		
		//Creiamo qualche User
		try {
			collection.createUser("Topolino","123");
			// collection.createUser("Topolino","123"); //ERR-> Utente gi� esistente
			collection.createUser("Paperino","456");
			collection.createUser("Pippo","789");
		}catch(Exception e) {
			//Gestiamo le eccezioni scrivendole in console
			System.out.println(e.getMessage());
		}
		
		
		//Controllo di getSize()
		try {
			//Prendiamo le dimensioni di un User ci aspettiamo che sia uguale a 0
			System.out.println("Size of Topolino: " + collection.getSize("Topolino", "123"));
			
			//Aggiungiamo qualche Stringa all'interno di un User
			collection.put("Topolino", "123", "Stringa1");
			collection.put("Topolino", "123", "Stringa2");
			collection.put("Topolino", "123", "Stringa3");
			collection.put("Topolino", "123", "Stringa4");
			
			//Riprendiamo le dimensioni nuovamente del solito User per controllare la corretta funzionalit� di getSize()
			System.out.println("Size of Topolino: " + collection.getSize("Topolino", "123"));
			
		}catch(Exception e) {
			//Gestiamo le eccezioni scrivendole in console
			System.out.println(e.getMessage());
		}
		
		
				
		//Controllo della corretta funzionalit� degli Iterator
		try {
			//Inizializzo l'iterator sull'User "Topolino"
			Iterator <String> tmp = collection.getIterator("Topolino", "123");
			System.out.println("\r---------- Iteratore di Topolino -----------");
		    while (tmp.hasNext()) {
		    	System.out.println(tmp.next());
		    }
		    System.out.println("------------ END -----------\r");
		}catch(Exception e) {
			//Gestiamo le eccezioni scrivendole in console
			System.out.println(e.getMessage());
		}
		
		
		
		//Rapido controllo con l'utilizzo di un oggetto diverso dalla Stringa ovvero il Book
		//Inizializzo una nuova collezione di SecureData passando come generico un Book
		SecureData<Book> bookCollection = new SecureData<Book>();
		
		try {
			//Creo un user
			bookCollection.createUser("Enzino","111");
			
			//Creo qualche Book
			Book b1 = new Book("Il suono in cui viviamo","Franco Fabbri","Music");
			Book b2 = new Book("Come funziona la musica","David Byrne","Music");
			
			//Li inserisco all'interno dell'user
			bookCollection.put("Enzino","111", b1);
			bookCollection.put("Enzino","111", b2);
			
		}catch(Exception e){
			//Gestiamo le eccezioni scrivendole in console
			System.out.println(e);
		}
		
		
		//Uso di nuovo l'iterator per controllare i risultati ottenuti
		try {
			Iterator <Book> tmp = bookCollection.getIterator("Enzino", "111");
			System.out.println("\r---------- Iteratore di Enzino -----------");
		    while (tmp.hasNext()) {
		    	System.out.println(tmp.next().toString());
		    }
		    System.out.println("----------- END ----------\r");

		}catch(Exception e) {
			//Gestiamo le eccezioni scrivendole in console
			System.err.println(e.getMessage());
		}
		
		
/*----------------------------------------SECONDA PARTE---------------------------------------------------*/
		
		
		System.out.println("<------------------------ SECONDA PARTE ------------------------->");
		//creiamo collection utilizzando la classe SecureDataTreeMap con Book
		SecureDataTreeMap<Book> bookTreeMap = new SecureDataTreeMap<Book>();
		
		//Eseguo alcune azioni di verifica
		try {
			
			//Creo un user
			bookTreeMap.createUser("Giovanni","1111");
			
			//Prendiamo le dimensioni di un Giovanni ci aspettiamo che sia uguale a 0
			System.out.println("Size of Giovanni: " + bookTreeMap.getSize("Giovanni", "1111"));
			
			//Creo qualche Book
			Book b2 = new Book("Come funziona la musica","David Byrne","Music");
			Book b3 = new Book("Fuoco e sangue","George R. R. Martin","Fantasy");
			Book b4 = new Book("Dragon Ball Super","Akira Toriyama","Manga");
			
			//Aggiungo a Giovanni un Book
			bookTreeMap.put("Giovanni","1111", b2);
			
			//Prendiamo le dimensioni di un Giovanni ci aspettiamo che sia uguale a 1
			System.out.println("Size of Giovanni: " + bookTreeMap.getSize("Giovanni", "1111"));
			
			
			//Cancello Giovanni
			bookTreeMap.RemoveUser("Giovanni", "1111");
			//Vari controlli sul Remove commentati per non bloccare il programma
			//System.out.println("Size of Giovanni: " + bookTreeMap.getSize("Giovanni", "1111"));  //--> Err: ID non trovato
			//bookTreeMap.put("Giovanni","1111", b2); //--> Err: ID non trovato
			//bookTreeMap.copy("Giovanni", "1111", b2); //--> Err: ID non trovato
			
			//Creo un altro User "Marco"
			bookTreeMap.createUser("Marco","1111");
			

			//Aggiungo i book alla collezione di "Marco"
			bookTreeMap.put("Marco","1111", b2);
			bookTreeMap.put("Marco","1111", b3);
			bookTreeMap.put("Marco","1111", b4);
			
			//Creo un ulteriore User
			bookTreeMap.createUser("Luca","1212");
			
			//TEST SHARE
			System.out.println("\r---------TEST SHARE----------");
			
			//SizeOf per vedere le dimensioni prima dello share
			System.out.println("Size of Luca: " + bookTreeMap.getSize("Luca", "1212"));
			
			//Utilizzo share per "copiare" un oggetto da marco a luca
			bookTreeMap.share("Marco","1111", "Luca", b3);
			
			//SizeOf per vedere le dimensioni dopo dello share
			System.out.println("Size of Luca: " + bookTreeMap.getSize("Luca", "1212"));
			
			System.out.println("---------END SHARE----------\r");
			
		}catch(Exception e){
			//Gestiamo le eccezioni scrivendole in console
			System.out.println(e.getMessage());
		}
		
		
		
		//ITERATOR BOOKTREEMAP MARCO + LUCA
		try {
			Iterator <Book> tmp_Marco = bookTreeMap.getIterator("Marco", "1111");
			System.out.println("\r---------- Iteratore di Marco -----------");
		    while (tmp_Marco.hasNext()) {
		    	System.out.println(tmp_Marco.next().toString());
		    }
		    System.out.println("----------- END ----------\r");
		    
			Iterator <Book> tmp_Luca = bookTreeMap.getIterator("Luca", "1212");
			System.out.println("\r---------- Iteratore di Luca -----------");
		    while (tmp_Luca.hasNext()) {
		    	System.out.println(tmp_Luca.next().toString());
		    }
		    System.out.println("----------- END ----------\r");

		}catch(Exception e) {
			System.out.println(e.getMessage());
		}

		
	}

}
